api = 2
core = 7.x

; Drupal core
; ------------
projects[drupal][type] = core
projects[drupal][version] = 7.22
projects[drupal][patch][] = "https://drupal.org/files/core-Bartik_overrides_unpublished_style_from_node.css-1854376-1.patch"
