<?php
/**
 * @file
 * Install, update and uninstall functions for this installation profile.
 */

/**
 * Implements hook_install().
 */
function shared_content_demo_install() {
  // Enable some standard blocks.
  $default_theme = variable_get('theme_default', 'bartik');
  $blocks = array(
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'powered-by',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 10,
      'region' => 'footer',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $default_theme,
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
  );
  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'));
  foreach ($blocks as $block) {
    $query->values($block);
  }
  $query->execute();

  // Allow visitor account creation with administrative approval.
  variable_set('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL);

  // Enable default permissions for system roles.
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content'));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content'));

  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 2;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();

  // Create a Home link in the main menu.
  $item = array(
    'link_title' => st('Home'),
    'link_path' => '<front>',
    'menu_name' => 'main-menu',
  );
  menu_link_save($item);

  // Update the menu router information.
  menu_rebuild();
}

/**
 * Implements hook_install_tasks().
 */
function shared_content_demo_install_tasks(&$install_state) {
  $tasks['shared_content_demo_config_form'] = array(
    'display_name' => st('Configure content deployment settings'),
    'type' => 'form',
  );

  return $tasks;
}

/**
 * Implements hook_install_tasks_alter().
 */
function shared_content_demo_install_tasks_alter(&$tasks, &$install_state) {
  $tasks['install_finished']['function'] = 'shared_content_demo_final_steps';
}

/**
 * Form constructor for this profile's configuration options.
 */
function shared_content_demo_config_form($form, &$form_state) {
  drupal_set_title(st('Configure content deployment settings'));

  $form['site_type'] = array(
    '#type' => 'radios',
    '#title' => st('Type of site'),
    '#description' => st('The content deployment system requires at least two sites, one to push content (the source site) and one to receive it (the destination site). Choose which one this is.'),
    '#options' => array(
      'source' => st('Source site'),
      'destination' => st('Destination site'),
    ),
    '#default_value' => 'source',
  );

  $form['other_site'] = array(
    '#type' => 'fieldset',
    '#title' => st('Other site information'),
    '#description' => st('Enter information about the other site that this one will communicate with.'),
    '#tree' => TRUE,
  );
  $form['other_site']['site_name'] = array(
    '#type' => 'textfield',
    '#title' => st('Site name'),
  );
  $form['other_site']['site_url'] = array(
    '#type' => 'textfield',
    '#title' => st('Site URL'),
    '#description' => st('Use the full site address (e.g.., "http://example.com").'),
  );

  $form['content_deployment_user'] = array(
    '#type' => 'fieldset',
    '#title' => st('Content deployment user account'),
    '#description' => st('Enter credentials for an account that will be created on the destination site and be contacted by the source site in order to deploy content. The credentials entered here must match what you enter on the other site you create.'),
    '#tree' => TRUE,
  );
  $form['content_deployment_user']['name'] = array(
    '#type' => 'textfield',
    '#title' => st('Username'),
    '#default_value' => 'content.deployment.user',
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#description' => st('Spaces are allowed; punctuation is not allowed except for periods, hyphens, and underscores.'),
    '#weight' => -10,
    '#attributes' => array('class' => array('username')),
  );
  $form['content_deployment_user']['mail'] = array(
    '#type' => 'textfield',
    '#title' => st('E-mail address'),
    '#default_value' => 'content.deployment.user@example.com',
    '#maxlength' => EMAIL_MAX_LENGTH,
    '#weight' => -5,
  );
  $form['content_deployment_user']['pass'] = array(
    '#type' => 'password',
    '#title' => st('Password'),
    '#size' => 25,
    '#weight' => 0,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Save and continue'),
    '#weight' => 15,
  );

  return $form;
}

/**
 * Validation handler for this profile's configuration options form.
 */
function shared_content_demo_config_form_validate($form, &$form_state) {
  // If at least one field was filled out, all of them must be.
  $fields = array(
    'other_site' => array('site_name', 'site_url'),
    'content_deployment_user' => array('name', 'mail', 'pass'),
  );
  $all_empty = TRUE;
  $all_filled = TRUE;
  $possible_errors = array();
  foreach ($fields as $group_name => $group_fields) {
    foreach ($group_fields as $field_name) {
      if (!empty($form_state['values'][$group_name][$field_name])) {
        $all_empty = FALSE;
      }
      else {
        $all_filled = FALSE;
        $possible_errors[$group_name][$field_name] = st('The %field field must be filled out unless all fields on the form are left empty.', array('%field' => $form[$group_name][$field_name]['#title']));
      }
    }
  }
  if (!$all_empty && !$all_filled) {
    foreach ($possible_errors as $group_name => $group_errors) {
      foreach ($group_errors as $field_name => $error) {
        form_error($form[$group_name][$field_name], $error);
      }
    }
  }

  // Perform standard validation of the name and e-mail address.
  if (!empty($form_state['values']['content_deployment_user']['name']) && ($error = user_validate_name($form_state['values']['content_deployment_user']['name']))) {
    form_error($form['content_deployment_user']['name'], $error);
  }
  if (!empty($form_state['values']['content_deployment_user']['mail']) && ($error = user_validate_mail($form_state['values']['content_deployment_user']['mail']))) {
    form_error($form['content_deployment_user']['mail'], $error);
  }
}

/**
 * Submit handler for this profile's configuration options form.
 */
function shared_content_demo_config_form_submit($form, &$form_state) {
  // Enable the example module.
  $module = $form_state['values']['site_type'] == 'source' ? 'shared_content_example_source' : 'shared_content_example_destination';
  module_enable(array($module));
  drupal_flush_all_caches();
  // Clear out unwanted messages from all the modules that were just enabled.
  drupal_get_messages('status');

  // Early return if there is no configuration.
  if (empty($form_state['values']['other_site']['site_name'])) {
    return;
  }

  if ($form_state['values']['site_type'] == 'source') {
    // Modify the deployment plan.
    $plan = deploy_plan_load('destination_site_plan');
    $plan->title = $form_state['values']['other_site']['site_name'];
    $plan->description = 'Deploys content to the ' .  $form_state['values']['other_site']['site_name'] . ' site.';
    deploy_plan_save($plan);
    // Modify the endpoint configuration.
    $endpoint = deploy_endpoint_load('destination_site_endpoint');
    $endpoint->title = $form_state['values']['other_site']['site_name'];
    $endpoint->description = 'Deployment endpoint for the ' .  $form_state['values']['other_site']['site_name'] . ' site.';
    $endpoint->authenticator_config['username'] = $form_state['values']['content_deployment_user']['name'];
    $endpoint->authenticator_config['password'] = $form_state['values']['content_deployment_user']['pass'];
    $endpoint->service_config['url'] = rtrim($form_state['values']['other_site']['site_url'], '/') . '/services/rest';
    ctools_export_crud_save('deploy_endpoints', $endpoint);
  }
  else {
    // Create the content deployment user account.
    $account = user_load(1);
    unset($account->uid);
    unset($account->uuid);
    $edit = $form_state['values']['content_deployment_user'];
    $edit['init'] = $edit['mail'];
    $edit['status'] = 1;
    $rid = (int) db_query("SELECT rid FROM {role} where name = 'content deployment user'")->fetchField();
    $edit['roles'][$rid] = 1;
    user_save($account, $edit);
  }

  // Save the submitted form values so we can use them later. See
  // shared_content_demo_final_steps().
  $values = &drupal_static('shared_content_demo_final_steps');
  $values = $form_state['values'];
}

/**
 * Custom replacement for the Drupal core "install finished" installation task.
 *
 * There is a Features caching issue that causes it to overwrite some of the
 * changes we'd like to make after the "shared_content_demo_config_form" form
 * is submitted (they get overwritten when caches are cleared at the end of
 * installation).
 *
 * This is an ugly hack to allow us to make those changes afterwards instead.
 */
function shared_content_demo_final_steps(&$install_state) {
  $output = install_finished($install_state);

  // Early return if there are no changes we need to make.
  $values = &drupal_static(__FUNCTION__, array());
  if (!$values) {
    return $output;
  }

  if ($values['site_type'] == 'source') {
    // Relabel the content deployment field.
    $field_name = 'field_deploy_to_destination_site';
    $field = field_info_field($field_name);
    foreach ($field['bundles'] as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {
        $instance = field_info_instance($entity_type, $field_name, $bundle);
        $instance['label'] = 'Deploy to ' . $values['other_site']['site_name'];
        field_update_instance($instance);
      }
    }
  }
  else {
    // Modify the UUID Redirect settings.
    variable_set('uuid_redirect_external_base_url', rtrim($values['other_site']['site_url'], '/'));
  }

  return $output;
}
