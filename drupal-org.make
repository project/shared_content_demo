api = 2
core = 7.x

; Modules
; --------
projects[ctools][subdir] = "contrib"
projects[ctools][version] = 1.3
projects[ctools][type] = "module"

projects[deploy][subdir] = "contrib"
projects[deploy][type] = "module"
projects[deploy][download][type] = "git"
projects[deploy][download][branch] = 7.x-2.x
projects[deploy][download][revision] = "ecb95681894e05a0d6b68f339835b842ae69b18f"
projects[deploy][patch][] = "https://drupal.org/files/deploy-undefined_variable_user-2052767-3.patch"

projects[deploy_services_client][subdir] = "contrib"
projects[deploy_services_client][version] = 1.0-beta2
projects[deploy_services_client][type] = "module"

projects[entity][subdir] = "contrib"
projects[entity][version] = 1.1
projects[entity][type] = "module"

projects[entity_dependency][subdir] = "contrib"
projects[entity_dependency][type] = "module"
projects[entity_dependency][download][type] = "git"
projects[entity_dependency][download][branch] = 7.x-1.x
projects[entity_dependency][download][revision] = "7ca711a5149083ca2aa3b6ada9d35cd5abc6c99a"

projects[features][subdir] = "contrib"
projects[features][version] = 2.0-rc1
projects[features][type] = "module"

projects[file_entity][subdir] = "contrib"
projects[file_entity][type] = "module"
projects[file_entity][download][type] = "git"
projects[file_entity][download][branch] = 7.x-2.x
projects[file_entity][download][revision] = "419319fb6f15cf773df7412ab95283b9d7ae905a"

projects[file_lock][subdir] = "contrib"
projects[file_lock][type] = "module"
projects[file_lock][download][type] = "git"
projects[file_lock][download][branch] = 7.x-2.x
projects[file_lock][download][revision] = "a84c0ed970fdd6e2d992d2ae50a5a13ce494f537"
projects[file_lock][patch][] = "https://drupal.org/files/file-lock-temporary-files-1514378-1.patch"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = 2.1
projects[libraries][type] = "module"

projects[media][subdir] = "contrib"
projects[media][type] = "module"
projects[media][download][type] = "git"
projects[media][download][branch] = 7.x-2.x
projects[media][download][revision] = "2319170eeb623e3e03a1e535875714979d75df72"
projects[media][patch][] = "https://drupal.org/files/media-filter-uuid-1578018-19.patch"

projects[services][subdir] = "contrib"
projects[services][version] = 3.4
projects[services][type] = "module"

projects[shared_content][subdir] = "contrib"
projects[shared_content][version] = 1.0-beta2
projects[shared_content][type] = "module"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = 2.0
projects[strongarm][type] = "module"

projects[uuid][subdir] = "contrib"
projects[uuid][type] = "module"
projects[uuid][download][type] = "git"
projects[uuid][download][branch] = 7.x-1.x
projects[uuid][download][revision] = "4bbf92d8c07485492c0f1065cb3647db492e1029"

projects[uuid_redirect][subdir] = "contrib"
projects[uuid_redirect][type] = "module"
projects[uuid_redirect][download][type] = "git"
projects[uuid_redirect][download][branch] = 7.x-1.x
projects[uuid_redirect][download][revision] = "ce1ab8491002078c995ee17d2bfcb314f88c547a"

projects[views][subdir] = "contrib"
projects[views][version] = 3.7
projects[views][type] = "module"

projects[workbench][subdir] = "contrib"
projects[workbench][version] = 1.2
projects[workbench][type] = "module"

projects[workbench_moderation][subdir] = "contrib"
projects[workbench_moderation][version] = 1.3
projects[workbench_moderation][type] = "module"
projects[workbench_moderation][patch][] = "https://drupal.org/files/double-messages-1237558-7.patch"
projects[workbench_moderation][patch][] = "https://drupal.org/files/workbench_moderation-entity-cache-1330562-22.patch"
projects[workbench_moderation][patch][] = "https://drupal.org/files/default-moderation-state-1408858-12.patch"
projects[workbench_moderation][patch][] = "https://drupal.org/files/workbench-moderation-1571614-1.patch"
projects[workbench_moderation][patch][] = "https://drupal.org/files/workbench-moderation-1436260-6.patch"
projects[workbench_moderation][patch][] = "https://drupal.org/files/workbench-moderation-draft-terminology-1732802-1.patch"

projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = 2.2
projects[wysiwyg][type] = "module"

; Libraries
; ---------
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
